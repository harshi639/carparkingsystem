OBJS = Vehicle.o History.o Validation.o Parking.o main.o
HEADER = Vehicle.h History.h Validation.h Parking.h 
SOURCE = Vehicle.cpp History.cpp Validation.cpp Parking.cpp main.cpp
OUT = parking.exe
CC = g++
FLAGS = -g -c -Wall

all:$(OBJS)
	$(CC) -g $(OBJS) -o $(OUT)

Vehicle.o: Vehicle.cpp
	$(CC) $(FLAGS) Vehicle.cpp

History.o: History.cpp
	$(CC) $(FLAGS) History.cpp

Validation.o: Validation.cpp
	$(CC) $(FLAGS) Validation.cpp

Parking.o: Parking.cpp
	$(CC) $(FLAGS) Parking.cpp

main.o: main.cpp
	$(CC) $(FLAGS) main.cpp

clean:
	del /f $(OBJS) $(OUT)