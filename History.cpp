#include "History.h"

// initializing static member 
int History::totalHistoryVehicles=0;

//constructor of History
History::History(std::string _vehicleParkingDate, std::string _vehicleNumber, std::string _vehicleEntryTime, std::string _vehicleExitTime, int _parkingTime,float _parkingCost ){
    vehicleParkingDate = _vehicleParkingDate;
    vehicleNumber = _vehicleNumber;
    vehicleEntryTime = _vehicleEntryTime;
    vehicleExitTime = _vehicleExitTime;
    parkingTime = _parkingTime;
    parkingCost = _parkingCost;

    totalHistoryVehicles++;
}

//destructor
History::~History(){
    totalHistoryVehicles--;
}

//return total number of vehicles in history
int History::getTotalHistoryVehicles(){
    return totalHistoryVehicles;
}

//return the vehicle parking date
std::string History::getHistoryVehicleDate(){
    return vehicleParkingDate;
}

//return the vehicle number
std::string History::getHistoryVehicleNumber(){
    return vehicleNumber;
}

//check if a vehicle is exited or not
bool History::isVehicleExited(){
    if (vehicleExitTime!="-" || vehicleExitTime.size()!=1){
        return true;
    }
    return false;
}

//return the details of a vehicles in history for saving
std::string History::getHistoryDetails(){
    return vehicleParkingDate+", "+vehicleNumber+", "+vehicleEntryTime+", "+vehicleExitTime+", "+std::to_string(parkingTime)+", "+std::to_string(parkingCost);
}

//return the details of a history vehicles for printing
std::string History::getPrintingHistoryDetails(){
    return vehicleParkingDate+"     "+vehicleNumber+"      "+vehicleEntryTime+"      "+vehicleExitTime+"       "+std::to_string(parkingTime)+"            " + char(156)+ " "+ std::to_string(parkingCost);
}
