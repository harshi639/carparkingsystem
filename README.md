# Car Parking system

Car parking system is build with c++ programming language majorly using the OOPs ,files and various other concepts. It is a console based application with a user friendly menu.
It reads its input from a file whose name is given as a command line arguement while executing the project.

## Compiling project

Compile the program with the help of Makefile

write following on bash shell:
```
make -f Makefile
```
## Executing project

Run by executing parking.exe as follows:

write following on bash shell:
```
./parking.exe <filename>
```
In above command, <filename> can be any name of the file which contains input data for the project. It is basically a command line arguement to specify the file name to read.
For example:
```
./parking.exe FILE.TXT
```

## Removing the files 
```
make clean
or 
make -f Makefile clean
```


## Testing
For testing, library which is used is [Catch2] (https://github.com/catchorg/Catch2/tree/v2.x).

To compile the project Test cases use following command:
```
make -f MakefileTesting
```
To run the project Test cases use following command:
```
./testing.exe
```
To clean/remove the project Test cases files use following command:
```
./make -f MakefileTesting clean
```
