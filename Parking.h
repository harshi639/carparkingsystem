#ifndef PARKING_H
#define PARKING_H

#include<iostream>
#include<string>
#include<fstream>
#include "Validation.h"
#include "Vehicle.h"
#include "History.h"

// Parking class used to store the vehicles in their respective data structures and performing other calculations and tasks
class Parking{
private:
    //data members of Parking
    int totalCurrentVehicle;
    int totalHistoryVehicle;
    Vehicle *vehicles[1000]; //Vehicle objects
    History *historyVehicles[1000];  //History objects
    std::string filename; //filename to read
    
public:
    //member functions: 

    //constructor of Parking
    Parking();

    //split the string into array according to seperator passed
    std::string *split(std::string st, std::string seperator);
    
    //removing leading and trailing spaces from string
    std::string strip(std::string );

    //set the filename to read
    void setFileName(std::string);

    //read data from specified File
    void readData();

    //printing all vehicles which have not exited 
    void printAllCurrentVehicles();

    //printing all vehicles which have exited 
    void printAllHistoryVehicles();
    
    //calculate total Parking time of a vehicle
    float calculateParkingTime(std::string, std::string);

    //calculate Parking cost of vehicle
    float calculateParkingCost(float);

    //output all vehicles details for a specific date
    void printAllVehiclesForSpecificDate();

    void  getAllTotal();
};

#endif