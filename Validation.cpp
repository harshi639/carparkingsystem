#include "Validation.h"

Validation::Validation(){
    //constructor
}

// implementation of validateInteger function
int Validation::validateInteger(std::string message)
{
    
    int input;
    std::cout << "\n" << message;
    
    // Numeric Values Validation Algorithm
    while ( (std::cin >> input).fail() || input < 1  )

    {
        // Error Message
        std::cout << "\nOops! Invalid Choice, Please Re-Enter a valid Numeric value: ";
        // Clear input stream
        std::cin.clear();
        // Discard previous input
        std::cin.ignore(1000, '\n');

    }
    return input;
}

// validating the correct format of date entered
std::string Validation::validateDateFormat(){
    //string to store date entered by user
    std::string date;
    std::cout<<"\nEnter the specific date in dd/mm/yyyy format (e.g. 09/03/2022) : ";
    std::cin >> date;

    //validating the format
    while(date.size()!=10 || date[2]!='/' || date[5]!='/'){
        std::cout<<"\nError: Please enter Date in Valid Format as specified above (dd/mm/yyyy):";
        std::cin>>date;
        
    }
    return date;
}