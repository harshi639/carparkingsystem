#ifndef HISTORY_H
#define HISTORY_H
#include<string>

// History data structure to store all the vehicles which have been exited
class History{
    private:
    // data members
        static int totalHistoryVehicles;

        std::string vehicleParkingDate;
        std::string vehicleNumber;
        std::string vehicleEntryTime;
        std::string vehicleExitTime;
        int parkingTime;  //stores parking time in seconds
        float parkingCost;
    
    public:
    // member functions:

        //default constructor
        History();

        //destructor
        ~History();
        
        //parameterized constructor
        History(std::string _vehicleParkingDate, std::string _vehicleNumber, std::string _vehicleEntryTime, std::string _vehicleExitTime, int _parkingTime,float _parkingCost );

        //return total number of vehicles in history
        int getTotalHistoryVehicles();
        
        //return the vehicle parked date
        std::string getHistoryVehicleDate();

        //return the vehicle number
        std::string getHistoryVehicleNumber();

        //check if vehicle have exited or not
        bool isVehicleExited( );
        
        //return the details of vehicle for saving
        std::string getHistoryDetails();

        //return the details of vehicle for printing
        std::string getPrintingHistoryDetails();


};

#endif