#ifndef VEHICLES_H
#define VEHICLES_H
#include<string>

// Vehicle data structure to store all the currently parked vehicles
class Vehicle{
    private:
    //data members
        static int totalCurrentVehicles; 

        std::string vehicleParkingDate;
        std::string vehicleNumber;
        std::string vehicleEntryTime;
        std::string vehicleExitTime;
    
    public:
    //member functions:

        //default constructor
        Vehicle();

        //destructor
        ~Vehicle();

        //parameterized constructor
        Vehicle(std::string _vehicleParkingDate, std::string _vehicleNumber, std::string _vehicleEntryTime, std::string _vehicleExitTime );

        //return total number of current vehicles
        int getTotalCurrentVehicles();

        //return the vehicle parking date
        std::string getVehicleDate();

        //return the vehicle number
        std::string getVehicleNumber();

        //check if vehicle is currently parked or it has been exited
        bool isVehicleCurrentlyParked( );


        //return the details of vehicle for saving
        std::string getDetails();

        //return the details of vehicle for printing 
        std::string getPrintingDetails();


};

#endif