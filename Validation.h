#ifndef VALIDATION_H
#define VALIDATION_H
#include <iostream>
#include <string>

class Validation
{
public:
    //constructor
    Validation();
    
    //validating the number
    int validateInteger(std::string message);

    //validating the date format
    std::string validateDateFormat();
};

#endif