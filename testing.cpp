#define CATCH_CONFIG_MAIN

#include <string>
#include "catch.hpp"
#include "Parking.h"
#include "Vehicle.h"
#include "History.h"


TEST_CASE("Get Total Count of Vehicles","[getTotalCurrentVehicles]"){
    //creating an object
    Vehicle *vehicleObj1 = new Vehicle("14/04/2022","ABCD 123","10:15:30","-");

    //checking total number of vehicles
    REQUIRE(vehicleObj1->getTotalCurrentVehicles() == 1);

    //creating one more object
    Vehicle *vehicleObj2 = new Vehicle("14/04/2022","ABCD 123","10:15:30","-");

    //checking total number of vehicles
    REQUIRE(vehicleObj2->getTotalCurrentVehicles() == 2);

    //creating an history object
    History *historyObj1 = new History("14/04/2022","ABCD 123","10:15:30","10:20:30",300,0);

    //checking total number of history vehicles
    REQUIRE(historyObj1->getTotalHistoryVehicles() == 1);

    //creating one more history object
    History *historyObj2 = new History("14/04/2022","XYZA 123","11:15:30","11:25:30",600,0);

    //checking total number of history vehicles
    REQUIRE(historyObj2->getTotalHistoryVehicles() == 2);
    
}

TEST_CASE("Checking Vehicle Currently Parked or Exited","[isVehicleCurrentlyParked]"){
    //creating objects
    Vehicle *vehicleObj = new Vehicle("14/04/2022","ABCD 123","10:15:30","-");
    History *historyObj = new History("14/04/2022","ABCD 123","10:15:30","11:30:30",4500,2.00);

    //checking vehicle is currently parked
    REQUIRE(vehicleObj->isVehicleCurrentlyParked()==true);

    //checking vehicle is exited
    REQUIRE(historyObj->isVehicleExited()==true);

}

TEST_CASE("Getting Current and History Details","[getDetails]"){
    //create object
    Vehicle *vehicleObj = new Vehicle("14/04/2022","XYZA 123","11:00:00","-");
    History *historyObj = new History("14/04/2022","XYZA 123","11:00:00","11:30:30",4500,2.00);

    //creating strings to compare
    std::string s1="14/04/2022, XYZA 123, 11:00:00,-";
    std::string s2="14/04/2022, XYZA 123, 11:00:00, 11:30:30, 4500, 2.000000";
    
    //checking current details
    REQUIRE(vehicleObj->getDetails()==s1);

    //checking history details
    REQUIRE(historyObj->getHistoryDetails()==s2);
}

TEST_CASE("Calculating Parking Time in Seconds","calculateParkingTime"){

    //creating object
    Parking park;

    int timeinsec = park.calculateParkingTime("10:15:30","11:15:30");

    REQUIRE(timeinsec == 3600);

}

TEST_CASE("Calculating Parking Cost","calculateParkingCost"){
    //creating object
    Parking park;

    //10 min time = 600 sec
    int timeinsec=600;
    REQUIRE(park.calculateParkingCost(timeinsec)==0);

    //40 min = 2400 sec
    timeinsec=2400;
    REQUIRE(park.calculateParkingCost(timeinsec)==1.50);

    //1 hr 30 min = 60*60 + 30*60 = 5400 sec
    timeinsec=5400;
    REQUIRE(park.calculateParkingCost(timeinsec)==3.00);

    //3 hours = 3*60*60= 10800 sec
    timeinsec=10800;
    REQUIRE(park.calculateParkingCost(timeinsec)==5.00);

    //6 hours = 21600 sec
    timeinsec=21600;
    REQUIRE(park.calculateParkingCost(timeinsec)==15.00);

    //10 hours = 36000 sec
    timeinsec=36000;
    REQUIRE(park.calculateParkingCost(timeinsec)==30.00);

}
