#include "Vehicle.h"

// initializing static member 
int Vehicle::totalCurrentVehicles=0;

Vehicle::Vehicle(){
    //vehicle Constructor
}

//destructor
Vehicle::~Vehicle(){
    
    totalCurrentVehicles--;
}

// constructor of Vehicle
Vehicle::Vehicle(std::string _vehicleParkingDate, std::string _vehicleNumber, std::string _vehicleEntryTime, std::string _vehicleExitTime){
    vehicleParkingDate = _vehicleParkingDate;
    vehicleNumber = _vehicleNumber;
    vehicleEntryTime = _vehicleEntryTime;
    vehicleExitTime = _vehicleExitTime;

    totalCurrentVehicles++;
}

//return total number of current vehicles
int Vehicle::getTotalCurrentVehicles(){
    return totalCurrentVehicles;
}

//return the vehicle parking date
std::string Vehicle::getVehicleDate(){
    return vehicleParkingDate;
}

//return the vehicle number
std::string Vehicle::getVehicleNumber(){
    return vehicleNumber;
}

//check if a vehicle is currently parked
bool Vehicle::isVehicleCurrentlyParked(){
    if (vehicleExitTime=="-" || vehicleExitTime.size()==1){
        return true;
    }
    return false;
}



//return the details of a vehicle
std::string Vehicle::getDetails(){
    return vehicleParkingDate+", "+vehicleNumber+", "+vehicleEntryTime+","+vehicleExitTime;
}

//return the details of a vehicle in more decorated way
std::string Vehicle::getPrintingDetails(){
    return vehicleParkingDate+"     "+vehicleNumber+"     "+vehicleEntryTime+"     "+vehicleExitTime;
}
