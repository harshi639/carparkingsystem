#include<iostream>
#include<string>
#include<conio.h>
#include "Validation.h"
#include "Parking.h"


// menu display 
int parkingMenu(Validation v){
        std::cout<<"\n\n";
        std::cout<<"***********************************\n";
        std::cout<<"**********<Parking Menu>***********\n";
        std::cout<<"***********************************\n";

        std::cout<<"-----------------------------------------------------\n";
        std::cout<<"1. Output all vehicles (with entry, exit times and cost) for a specific date \n";
        std::cout<<"2. Read Data from File\n";
        std::cout<<"3. List all Vehicles which are currently parked\n";
        std::cout<<"4. List all Vehicles in History (which have exited)\n";
        std::cout<<"5. Exit !\n";
        std::cout<<"-----------------------------------------------------\n";

        // validating input of user
        return v.validateInteger("Select your choice:");

}

// main function with command line arguement
int main(int argc, char ** arg){

    //numeric Validation object
    Validation v;
    
    
//Parking object
    Parking park;

    //checking command line arguement
    if (argc==1){

        std::cout<<"\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
        std::cout<<"\nError: You have not entered the File name in command line";
        std::cout<<"\nPlease Re-run!! and specify the File name to read";
        std::cout<<"\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
        return 0;
    }
    else {
        park.setFileName(arg[1]);    
    }
    
    //reading the file 
    park.readData();

    while(true){

        //taking valid input of user after displaying menu
        int choice = parkingMenu(v);

        switch(choice){
            case 1:{
                //printing all vehicles belonging to a specific date
                park.printAllVehiclesForSpecificDate();
                std::cout<<"\n\nPress Enter to Continue...";
                getch();
                break;
            }
            case 2:{
                //reading data from file
                park.readData();
                std::cout<<"\n\nPress Enter to Continue...";
                getch();
                break;
            }

            case 3:{
                //printing all vehicles which have not exited
                park.printAllCurrentVehicles();

                std::cout<<"\n\nPress Enter to Continue...";
                getch();
                break;
            }
            case 4:{
                //printing all vehicles from history with their parking time and cost
                park.printAllHistoryVehicles();
                std::cout<<"\n\nPress Enter to Continue...";
                getch();
                break;

            }
            
            case 5:{
                //exiting the program
                std::cout<<"\n\nExiting the Parking System  \nBye !!\n";
                return 0;
            }
            case 6:{

                park.getAllTotal();
                getch();
                break;
            }
            default:{
                std::cout<<"\nError:Invalid Choice , press any key to continue...";
                getch();
            }
        }
        
    }
    
    return 0;
}
