#include "Parking.h"

//parking constructor
Parking::Parking(){
    
    totalCurrentVehicle=vehicles[0]->getTotalCurrentVehicles();

    totalHistoryVehicle=historyVehicles[0]->getTotalHistoryVehicles();
}

//set the file name specified in command line
void Parking::setFileName(std::string fn){
    filename=fn;
}



//printing all vehicles which have not exited
void Parking::printAllCurrentVehicles(){
    int count = 0 ;
    //printing heading to make it more readable and decorative
    std::cout<<"\n\n--------------------------------------------------------";
    std::cout<<"\nSno.  "<<"ParkingDate  "<<"VehicleNumber  "<<"EntryTime  "<<"ExitTime";
    std::cout<<"\n--------------------------------------------------------";
    while( count < totalCurrentVehicle){
        std::cout<<"\n"<<count+1<<"     "<<vehicles[count]->getPrintingDetails();
        count++;
    }
}

//printing all vehicles which have exited with their parking time and cost
void Parking::printAllHistoryVehicles(){
    int count = 0 ;
    //printing heading to make it more readable and decorative
    std::cout<<"\n---------------------------------------------------------------------------------------------";
    std::cout<<"\nSno.  "<<"ParkingDate  "<<"VehicleNumber  "<<"EntryTime      "<<"ExitTime  "<<"ParkingTime(in sec)  "<<"ParkingCost";
    std::cout<<"\n---------------------------------------------------------------------------------------------";
    while( count < totalHistoryVehicle){
        std::cout<<"\n"<<count+1<<"     "<<historyVehicles[count]->getPrintingHistoryDetails();
        count++;
    }
}

// print vehicles details for a specific date
void Parking::printAllVehiclesForSpecificDate(){
    bool flag1=0,flag2=0;
    //string to store date entered by user
    std::string date;
    
    Validation v;
    date = v.validateDateFormat();
    
    //printing heading to make it more readable and decorative
    std::cout<<"\n=================================================================================================";
    std::cout<<"\nVehicles which are currently parked as per entered date:("<<date<<") are:-";
    std::cout<<"\n--------------------------------------------------------";
    std::cout<<"\nSno.  "<<"ParkingDate  "<<"VehicleNumber  "<<"EntryTime  "<<"ExitTime";
    std::cout<<"\n--------------------------------------------------------";
    int count = 0 ;
    while(count < totalCurrentVehicle){
        std::string getDate = vehicles[count]->getVehicleDate();

        if (date==getDate){
            //set the flag if we find some record
            flag1=true;
            std::cout<<"\n"<<count+1<<"     "<<vehicles[count]->getPrintingDetails();
        }
        count++;
    }
    if (flag1==false){
        std::cout<<"\n<--No current data available for specified date i.e.("<<date<<")-->";
        std::cout<<"\n<------Please Recheck your date------>";

    }

    count=0;
    //printing heading to make it more readable and decorative
    std::cout<<"\n\n\nHistory Vehicles which are exited as per entered date ("<<date<<") are:-";
    std::cout<<"\n---------------------------------------------------------------------------------------------";
    std::cout<<"\nSno.  "<<"ParkingDate  "<<"VehicleNumber  "<<"EntryTime      "<<"ExitTime  "<<"ParkingTime(in sec)  "<<"ParkingCost";
    std::cout<<"\n---------------------------------------------------------------------------------------------";
    while(count < totalHistoryVehicle){
        std::string getHistoryDate = historyVehicles[count]->getHistoryVehicleDate();

        if (date==getHistoryDate){
            //set the flag if we find some record
            flag2=true;
            std::cout<<"\n"<<count+1<<"     "<<historyVehicles[count]->getPrintingHistoryDetails();
        }
        count++;
    }
    if (flag2==false){
        std::cout<<"\n<--No history data available for specified date i.e.("<<date<<")-->";
        std::cout<<"\n<------Please Recheck your date------>";
    }
}

// return total parking Time of vehicle in seconds 
float Parking::calculateParkingTime(std::string _entry, std::string _exit){
    std::string *entryTime=split(_entry,":");
    std::string *exitTime=split(_exit,":");

    int entryHour = stoi(entryTime[0]);
    int entryMinute = stoi(entryTime[1]);
    int entrySecond = stoi(entryTime[2]);

    int exitHour = stoi(exitTime[0]);
    int exitMinute = stoi(exitTime[1]);
    int exitSecond = stoi(exitTime[2]);

    //finding parking time i.e. difference between entry and exit times in seconds
    float parkingTime = (exitHour*60*60 + exitMinute*60 + exitSecond)-(entryHour*60*60 + entryMinute*60 + entrySecond);

    return parkingTime;
    
}

//calculate parking cost as per parking time
float Parking::calculateParkingCost(float time){
    // if parking time < 30 mins(i.e. 30*60 seconds) then cost = 0
    if (time <= 30*60){
        return 0;
    }
    //if parking time is upto 1 hour(i.e. 1*60*60 seconds) then cost = £ 1.50
    else if (time <=1*60*60){
        return 1.50;
    }
    // if parking time is upto 2 hours(i.e. 2*60*60 seconds) then cost = £ 3.00
    else if (time<=2*60*60){
        return 3.00;
    }
    // if parking time is upto 4 hours(i.e. 4*60*60 seconds) then cost = £ 5.00
    else if (time<=4*60*60){
        return 5.00;
    }
    // if parking time is upto 8 hours(i.e. 8*60*60 seconds) then cost = £ 15.00
    else if (time<=8*60*60){
        return 15.00;
    }

    // if parking time is more than 8 hours(i.e. 8*60*60 seconds) then cost = £ 30.00
    else {
        return 30.00;
    }
}

//Reading data from file and adding it to suitable data structure
void Parking::readData(){
    for (int i = 0; i < totalCurrentVehicle; i++)
    {   
        //deleting objects and calling destructor
        delete vehicles[i];
    }
    for (int i = 0; i < totalHistoryVehicle; i++)
    {
        //deleting objects and calling destructor
        delete historyVehicles[i];
    }

    totalCurrentVehicle=0;
    totalHistoryVehicle=0;

    std::ifstream parkingFile;
    
    //opening the file
    parkingFile.open(filename);

    if (!parkingFile){
        std::cout<<"\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
        std::cout<<"\nError: Your File ("<<filename<<") could not be found";
        std::cout<<"\nPlease make sure the file is present in current Directory/Folder";
        std::cout<<"\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
        exit(1);
    }

    std::cout<<"\nReading data starts from "<<filename;

    //create string to store first header line of file
    std::string header ;
    getline(parkingFile, header);
    // std::cout<<"\n"<<header<<"\n";

    //create string to store lines of file
    std::string veh;
    while(getline(parkingFile,veh)){

        std::string *ob;
        //split string into array
        ob = split(veh ,",");
        std::string _date=strip(ob[0]);
        std::string _vehicleno=strip(ob[1]);
        std::string _entry=strip(ob[2]);
        std::string _exit=strip(ob[3]);

        // creating objects
        if (_exit=="-" || _exit.size()==1){
            vehicles[totalCurrentVehicle]= new Vehicle(_date, _vehicleno, _entry, _exit);
            totalCurrentVehicle++;
        }
        else{
            //calculating parking time
            float _parkingTime = calculateParkingTime(_entry, _exit);

            //calculating parking cost 
            float _parkingCost = calculateParkingCost(_parkingTime);

            historyVehicles[totalHistoryVehicle] = new History(_date, _vehicleno, _entry, _exit,_parkingTime, _parkingCost);
            totalHistoryVehicle++;
        }

    }
    std::cout<<"\nSuccessfully read the data from file";
}

// split std::string into array
std::string *Parking::split(std::string str, std::string seperator)
{
    // ans std::string
    std::string *ans = new std::string[100];
    int count = 0;
    int start = 0;
    // finding the seperator
    int end = str.find(seperator);
    // find and split std::string into array
    while (end != -1)
    {
        ans[count] = str.substr(start, end - start);
        count++;
        start = end + seperator.size();
        end = str.find(seperator, start);
    }
    // addingg to aray ansput
    ans[count] = str.substr(start, end - start);
    // return
    return ans;
}

//removing leading and trailing whitespaces
std::string Parking::strip(const std::string s){
    int len=s.size();
    int start=0;
    int end=len-1;
    while(start<end && s[start]==' '){
        start++;
    }
    while(end>start && s[end]==' '){
        end--;
    }

    return s.substr(start,end-start+1);
}

void Parking::getAllTotal(){
    std::cout<<vehicles[0]->getTotalCurrentVehicles()<<"\n";

    std::cout<<historyVehicles[0]->getTotalHistoryVehicles()<<"\n";
}